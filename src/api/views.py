import json
import pandas as pd
from datetime import datetime

from sklearn.linear_model import LinearRegression, Lasso
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor

from django.views.generic import TemplateView
from django.http import HttpResponse

from api.data import get_data_from_db, get_data_from_url

# sklearn models
models = {
    "random_forest": RandomForestRegressor(),
    "gradient_boosting": GradientBoostingRegressor(),
    "lasso": Lasso(),
    "regression": LinearRegression()
}

class Home(TemplateView):
    template_name = 'home.html'

def is_all_param(param_list):
    """Check is all params has value"""
    return all(param_list) 


def http_response_json(response={}):
    return HttpResponse(
        json.dumps(response, indent=4),
        content_type="application/json"
    )

def predict(request):
    """API View: /api/predict"""
    model = request.GET.get('model', None)
    target = request.GET.get('target', None)
    test_to = request.GET.get('test_to', None)
    test_from = request.GET.get('test_from', None)

    # Check all required params available or not
    param_list = [model, target, test_to, test_from]
    if not is_all_param(param_list):
        return http_response_json({'Status':'Error'})

    # Converting string to date
    to_dt = datetime.strptime(test_to, '%Y%m%d')
    from_dt = datetime.strptime(test_from, '%Y%m%d')
    
    # Get data from DB
    df = get_data_from_db()
    # Get data from URLs
    #df = get_data_from_url()

    x_test = df.drop([target, "Date", "Hours"], axis=1)
    x_test = x_test[(df['Date'] >= from_dt) & (df['Date'] <= to_dt)]
    x_train = df.drop([target, "Date", "Hours"], axis=1)
    x_train = x_train.drop(x_test.index, axis=0)
    y_train = df[target].reindex(x_train.index)
    y_test = df[target].reindex(x_test.index)

    models[model].fit(x_train, y_train)
    predictions = pd.DataFrame(index=x_test.index)
    predictions[model] = models[model].predict(x_test)
    return http_response_json(predictions.to_dict())


def train(request):
    """API View: /api/train"""
    model = request.GET.get('model', None)
    target = request.GET.get('target', None)
    train_to = request.GET.get('train_to', None)
    train_from = request.GET.get('train_from', None)

    # Check all required params available or not
    param_list = [model, target, train_to, train_from]
    if not is_all_param(param_list):
        return http_response_json({'Status':'Error'})
    to_dt = datetime.strptime(train_to, '%Y%m%d')
    from_dt = datetime.strptime(train_from, '%Y%m%d')
    features = request.GET.getlist('features', None)

    # Get data from DB
    df = get_data_from_db()
    # Get data from URLs
    #df = get_data_from_url()
    
    x_train = df.drop([target, "Date", "Hours"], axis=1)
    x_train = x_train[(df['Date'] >= from_dt) & (df['Date'] <= to_dt)]
    x_test = df.drop([target, "Date", "Hours"], axis=1)
    x_test = x_test.drop(x_train.index, axis=0)
    y_train = df[target].reindex(x_train.index)
    y_test = df[target].reindex(x_test.index)

    # TODO change this accordingly. Not sure what this features list is doing
    if features:
        # Using data only for features list
        x_train = x_train[features]
        x_test = x_test[features]

    models[model].fit(x_train, y_train)
    predictions = pd.DataFrame(index=x_test.index)
    predictions[model] = models[model].predict(x_test)
    return http_response_json(predictions.to_dict())

