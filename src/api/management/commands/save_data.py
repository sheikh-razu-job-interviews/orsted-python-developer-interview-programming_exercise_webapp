"""Read data from URLs and save in DB"""

from django.core.management.base import BaseCommand
from api.data import save_market_data


class Command(BaseCommand):
    help = "Save Market Data from URLs" 

    def handle(self, *args, **options):
        save_market_data()
        print("Successfully saved data in Database")