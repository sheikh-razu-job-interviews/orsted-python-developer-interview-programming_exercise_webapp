import pandas as pd
from datetime import datetime

from api.models import Marketdata


url_2020 = "https://www.nordpoolgroup.com/4adabc/globalassets/marketdata-excel-files/elspot-prices_2020_hourly_eur.xls"
url_2019 = "https://www.nordpoolgroup.com/4adabc/globalassets/marketdata-excel-files/elspot-prices_2019_hourly_eur.xls"

var_map = {
    'Date': 'date',
    'Hours': 'hours',
    'SYS': 'sys',
    'SE1': 'se1',
    'SE2': 'se2',
    'SE3': 'se3',
    'SE4': 'se4',
    'FI': 'fi',
    'DK1': 'dk1',
    'DK2': 'dk2',
    'Oslo': 'oslo',
    'Kr.sand': 'kr_sand',
    'Bergen': 'bergen',
    'Molde': 'molde',
    'Tr.heim': 'tr_heim',
    'TromsÃ¸': 'tromsa',
    'EE': 'ee',
    'LV': 'lv',
    'LT': 'lt',
    'AT': 'at',
    'BE': 'be',
    'DE-LU': 'de_lu',
    'FR': 'fr',
    'NL': 'nl'
}


def get_mapped_data(data_dict):
    mapped_data_list = []
    for i in range(len(data_dict)):
        mapped_data = {'index': i}
        data = data_dict[i]
        for key, val in data.items():
            if key == 'Date':
                val = datetime.strptime(val, '%d-%m-%Y').date()
            mapped_data.update({var_map.get(key):val})
        mapped_data_list.append(mapped_data)
    return mapped_data_list


def save_market_data():
    """Read data from URLs and saved each entry in Database"""

    # get data
    print("Reading data for 2020")
    df_2020 = pd.read_html(url_2020, decimal=",", thousands=".")[0]
    df_2020.columns = [c[-1] for c in df_2020.columns]
    df_2020 = df_2020.rename(columns={"Unnamed: 0_level_2": "Date"})

    print("Reading data for 2019")
    df_2019 = pd.read_html(url_2019, decimal=",", thousands=".")[0]
    df_2019.columns = [c[-1] for c in df_2019.columns]
    df_2019 = df_2019.rename(columns={"Unnamed: 0_level_2": "Date"})

    df = df_2019.append(df_2020)

    # fill nans
    for col in df:
        if f"{col}.1" in df.columns:
            df[col] = df[col].fillna(df[f"{col}.1"])
            df = df.drop(f"{col}.1", axis=1)
    df = df.dropna().reset_index(drop=True)

    # Mapping data to adjust with Django model field names
    mapped_data = get_mapped_data(df.to_dict('index'))
    
    print(f"Saving data in database")
    print(f"Found {len(mapped_data)} rows of data")
    for data in mapped_data:
        obj, created = Marketdata.objects.update_or_create(
            index=data['index'],
            defaults=data
        )
        obj.save()
        if data['index']%100 == 0:
            print(f"Saved {data['index']} entries in the database")

def get_data_from_db():
    """Retrieve data from DB and convert it into Dataframe"""
    df = pd.DataFrame(list(Marketdata.objects.all().values()))
    # Removing unwanted columns
    df = df.drop(['id', 'index'], axis=1)
    # Renaming columns to original names
    df = df.rename(columns={v: k for k, v in var_map.items()})
    # Converting Date values to datetime object
    df['Date']= pd.to_datetime(df['Date'])
    return df

def get_data_from_url():
    """Get data directly reading URLs
    
    Slow loading
    """
    print("Reading data for 2020")
    df_2020 = pd.read_html(url_2020, decimal=",", thousands=".")[0]
    df_2020.columns = [c[-1] for c in df_2020.columns]
    df_2020 = df_2020.rename(columns={"Unnamed: 0_level_2": "Date"})

    print("Reading data for 2019")
    df_2019 = pd.read_html(url_2019, decimal=",", thousands=".")[0]
    df_2019.columns = [c[-1] for c in df_2019.columns]
    df_2019 = df_2019.rename(columns={"Unnamed: 0_level_2": "Date"})

    df = df_2019.append(df_2020)

    # fill nans
    for col in df:
        if f"{col}.1" in df.columns:
            df[col] = df[col].fillna(df[f"{col}.1"])
            df = df.drop(f"{col}.1", axis=1)
    df = df.dropna().reset_index(drop=True)

    # Converting Date values to datetime object
    df['Date']= pd.to_datetime(df['Date'])
    return df
