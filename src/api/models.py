from django.db import models

# Create your models here.
class Marketdata(models.Model):
    index = models.PositiveIntegerField(unique=True)
    date = models.DateField()
    hours = models.CharField(max_length=10)
    sys = models.FloatField(max_length=10)
    se1 = models.FloatField(max_length=10)
    se2 = models.FloatField(max_length=10)
    se3 = models.FloatField(max_length=10)
    se4 = models.FloatField(max_length=10)
    fi = models.FloatField(max_length=10)
    dk1 = models.FloatField(max_length=10)
    dk2 = models.FloatField(max_length=10)
    oslo = models.FloatField(max_length=10)
    kr_sand = models.FloatField(max_length=10)
    bergen = models.FloatField(max_length=10)
    molde = models.FloatField(max_length=10)
    tr_heim = models.FloatField(max_length=10)
    tromsa = models.FloatField(max_length=10)
    ee = models.FloatField(max_length=10)
    lv = models.FloatField(max_length=10)
    lt = models.FloatField(max_length=10)
    at = models.FloatField(max_length=10)
    be = models.FloatField(max_length=10)
    de_lu = models.FloatField(max_length=10)
    fr = models.FloatField(max_length=10)
    nl = models.FloatField(max_length=10)