#! /bin/bash

OS="`uname -s`"

echo "$OS"
# Creates overlay network shared by the different Wasabi services
if [ "$OS" = "Darwin" ]; then
    docker network create -d overlay --subnet=10.0.3.0/24 --scope=swarm orsted
    # docker network create --subnet=10.0.8.0/24 orsted
elif [ "$OS" = "Linux" ]; then
    #docker network create -d overlay --subnet=10.0.10.0/24 --scope=swarm orsted
    docker network create -d overlay orsted
else
    echo "Unknown OS ($OS) cannot create correct network"
fi

