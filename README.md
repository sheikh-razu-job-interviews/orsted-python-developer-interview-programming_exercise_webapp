Task: https://gitlab.com/edsim/interview-tests/-/blob/master/programming_exercise_webapp.ipynb

### Notes for the exercise: Develop a complete python web-app

`This project is designed to be run in Linux or MAC OS. I haven't test this project
in Windows, but simply changing some commands can make this project running in Windows too.`

I have used Docker and Docker Swarm to deploy the server.

This project is based on Django and MySQL. However, I haven't used MySQL to
save the data, instead I have used SQLITE as I want to share this project ready
with data in the database.

I have used a database because, I can see that, reading the XLS files directly
from URLs and processing by pandas takes way long time. And, serving the data in
API, loading time will be very long. Therefore, I have saved the data first in
the database so that the API can serve the data from Database.

However, if you still want to directly use the URLs, you simply need
to use `get_data_from_url()` function in [views.py](src/api/views.py)

##### Main working files:

- [data.py](src/api/data.py)
- [views.py](src/api/views.py)
- [models.py](src/api/models.py)

##### API URLs

- [train](http://localhost:1234/api/train?model=random_forest&target=FR&train_from=20200101&train_to=20200301&features=DE-LU&features=NL&features=BE)
- [predict](http://localhost:1234/api/predict?model=random_forest&target=FR&test_from=20200302&test_to=20200315)

##### Home of the web-app

[Web-app](http://localhost:1234)

### Docker setup

#### Build MySql docker image

`docker build -f docker/001-Dockerfile.mysql -t orsted-mysql:latest .`


#### Build App docker image

`docker build -f docker/002-Dockerfile.app -t orsted-app:latest .`


#### Init Swarm

`docker swarm init`


### Create local volumes

`sh create-dev-folders.sh`


### Create docker network

Before running this command, make sure ** --subnet=10.0.3.0/24 ** is not in used.
You may change the subnet ip address if you want.

`sh create-network.sh`


### Deploying in Swarm

Use any of the following command

`sh dpl-orsted.sh` or `docker stack deploy -c docker/compose-dev-orsted.yml orsted`

By default, the development server (modwsgi) will be run in the container. 

For production change [Compose file](docker/compose-dev-orsted.yml) to run this system using Apache.

After deploy, wait for the service ready. And then visit the Web app.

[Web-app](http://localhost:1234)

Docker services (`docker service ls`) should look as follows.

```
ID                  NAME                MODE                REPLICAS            IMAGE                 PORTS
xjizc12ndk6l        orsted_database     replicated          1/1                 orsted-mysql:latest   *:3306->3306/tcp
jgpibw4admhu        orsted_ui           replicated          1/1                 orsted-app:latest     *:1234->8020/tcp
```

### Using MySQl or SQLite

By default this system is using SQLite. So that I can share the data in a file (db.sqlite3).
However, if you want to use MYSQL, you need to change the setting.py in `/src/orsted/settings.py`

[setting.py](src/orsted/settings.py)


Following code should be un-commented

```
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'HOST': e("ORSTED_DB_HOST"),
#         'NAME': e("ORSTED_DB_DATABASE"),
#         'USER': e("ORSTED_DB_USERNAME"),
#         'PASSWORD': e("ORSTED_DB_PASSWORD"),
#         'PORT':     e("ORSTED_DB_PORT"),
#     }
# }
```

And following code should be comment

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```

At the end, you make sure all database migrations are migrated

- `python shell.py`
- `python manage.py migrate`


### Saving data in Database

Data is saved in the database (SQLite). However, in case if you want to
save data in MySQL you can run following commands.

First login to Docker container and run a Django management command. 

- `python shell.py`
- `python manage.py save_data`







