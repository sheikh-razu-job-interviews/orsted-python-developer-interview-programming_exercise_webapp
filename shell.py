#! /usr/bin/env python3

"""
Script to automatically login to the running management
container for orsted ui
"""

import subprocess, sys, re

name = "orsted_ui"
cmd = "bash"

stdout = subprocess.getoutput("docker ps")
c = 0
for l in stdout.split("\n"):
    if c>0:
        ts = re.split(r"\s{2,}", l)
        if name in ts[len(ts)-1]:
            subprocess.call("docker exec -it {} {}".format(ts[0], cmd), shell=True)
            sys.exit(0)
    c += 1
print("Service {} is not running!?".format(name))
